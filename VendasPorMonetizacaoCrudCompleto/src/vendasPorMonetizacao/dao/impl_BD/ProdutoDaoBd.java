/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vendasPorMonetizacao.dao.impl_BD;



import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import vendasPorMonetizacao.dominio.Produto;
import vendaPorMonetizacao.dao.ProdutoDao;
/**
 *
 * @author thiago
 */
public class ProdutoDaoBd extends DaoBd<Produto> implements ProdutoDao {

 
    
    
    
    @Override
  public void salvar(Produto produto) {
        int id = 0;
        
        
        try {
            String sql = "INSERT INTO produto (nome, preco) "
                    + "VALUES (?,?)";

            
conectarObtendoId(sql);
            comando.setString(1, produto.getNome());
            comando.setDouble(2, produto.getPreco());
            
            comando.executeUpdate();
            
       
            
              ResultSet resultado = comando.getGeneratedKeys();
              
            if (resultado.next()) {
                
                id = resultado.getInt(1);
                produto.setId(id);
            }
            else{
                System.err.println("Erro de Sistema - Nao gerou o id conforme esperado!");
                throw new BDException("Nao gerou o id conforme esperado!");
            }

        } catch (SQLException ex) {
            System.err.println("Erro de Sistema - Problema ao salvar cliente no Banco de Dados!");
            throw new BDException(ex);
        } finally {
            fecharConexao();
        }
    }
    
    @Override
    public void deletar(Produto produto) {
        try {
            String sql = "DELETE FROM produto WHERE id = ?";

            conectar(sql);
            comando.setInt(1, produto.getId());
            comando.executeUpdate();

        } catch (SQLException ex) {
            System.err.println("Erro de Sistema - Problema ao deletar produto no Banco de Dados!");
            throw new RuntimeException(ex);
        } finally {
            fecharConexao();
        }

    }

    @Override
    public void atualizar(Produto produto) {
        try {
            String sql = "UPDATE produto SET id=?, nome=?, preco=? "
                    + "WHERE id=?";

            conectar(sql);
            comando.setInt(1, produto.getId());
            comando.setString(2, produto.getNome());
            comando.setDouble(3, produto.getPreco());
            
            
            comando.setInt(4, produto.getId());
            comando.executeUpdate();

        } catch (SQLException ex) {
            System.err.println("Erro de Sistema - Problema ao atualizar produto no Banco de Dados!");
            throw new BDException(ex);
        } finally {
            fecharConexao();
        }
    }

    
@Override
    public List<Produto> listar() {
        List<Produto> listaProdutos = new ArrayList<>();

        String sql = "SELECT * FROM produto";

        try {
            conectar(sql);

            ResultSet resultado = comando.executeQuery();

            while (resultado.next()) {
                int id = resultado.getInt("id");
                String nome = resultado.getString("nome");
                double preco = resultado.getDouble("preco");

                
                

                Produto prod = new Produto(id, nome, preco);
                listaProdutos.add(prod);

            }
        } catch (SQLException ex) {
            System.err.println("Erro de Sistema - Problema ao buscar os produtos do Banco de Dados!");
            throw new BDException(ex);
        } finally {
            fecharConexao();
        }
        return (listaProdutos);
    }
@Override
    public List<Produto> listarPorNome(String nome) {
        List<Produto> listaProdutos = new ArrayList<>();
        String sql = "SELECT * FROM produto WHERE nome LIKE ?";

        try {
            conectar(sql);
            comando.setString(1, "%" + nome + "%");
            ResultSet resultado = comando.executeQuery();

            while (resultado.next()) {
                int id = resultado.getInt("id");
                String nomeX = resultado.getString("nome");
                Double preco = resultado.getDouble("preco");
                

                Produto prod = new Produto(id,nomeX,preco);

                listaProdutos.add(prod);

            }

        } catch (SQLException ex) {
            System.err.println("Erro de Sistema - Problema ao buscar os produtos pelo nome do Banco de Dados!");
            throw new BDException(ex);
        } finally {
            fecharConexao();
        }
        return (listaProdutos);
    }

    @Override
    public Produto procurarPorId(int id) {
        String sql = "SELECT * FROM produto WHERE id = ?";

        try {
            conectar(sql);
            comando.setInt(1, id);

            ResultSet resultado = comando.executeQuery();

            if (resultado.next()) {
               
                String nome = resultado.getString("nome");
                double preco = resultado.getDouble("preco");
                
                

                Produto prod = new Produto(id,nome,preco);

                return prod;

            }

        } catch (SQLException ex) {
            System.err.println("Erro de Sistema - Problema ao buscar o produto pelo id do Banco de Dados!");
            throw new BDException(ex);
        } finally {
            fecharConexao();
        }

        return (null);
    }

    @Override
    public List<Produto> listarPorIdProdutos() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    

    




  
    


}
