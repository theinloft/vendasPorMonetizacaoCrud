/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vendasPorMonetizacao.negocio;

import java.util.List;
import vendaPorMonetizacao.dao.ProdutoDao;
import vendasPorMonetizacao.negocio.NegocioException;
import vendasPorMonetizacao.dao.impl_BD.ProdutoDaoBd;
import vendasPorMonetizacao.dominio.Cliente;
import vendasPorMonetizacao.dominio.Produto;

/**
 *
 * @author thiago
 */
public class ProdutoNegocio {
    private ProdutoDao produtoDao = new ProdutoDaoBd();
    
    public void salvar(Produto p) throws NegocioException {
        this.validarCamposObrigatorios(p);
        produtoDao.salvar(p);
    }

    public Produto procurarPorId(int id) throws NegocioException {
        if (id < 0){
            throw new NegocioException("Campo Código nao informado");
        }
        Produto produto = produtoDao.procurarPorId(id);
        
        if (produto == null) {
            throw new NegocioException("produto nao encontrado");
        }
        return (produto);
    }

    public void deletar(Produto prod) throws NegocioException {
        if (prod.getId()< 0) {
            throw new NegocioException("Produto nao existe!");
        }
        produtoDao.deletar(prod);
    }

    public void atualizar(Produto prod) throws NegocioException {
        if (prod == null || prod.getId()< 0) {
            throw new NegocioException("Produto nao existe!");
        }
        
        produtoDao.atualizar(prod);
    }

    public List<Produto> listarPorNome(String nome) throws NegocioException {
       if (nome == null || nome.isEmpty()) {
            throw new NegocioException("Campo nome nao informado");
        }
        return(produtoDao.listarPorNome(nome));
    }


    public List<Produto> listar() {
        return (produtoDao.listar());
    }
    
    private void validarCamposObrigatorios(Produto p) throws NegocioException {
        if (p.getId()< 0) {
            throw new NegocioException("Campo Código inválido");
        }

    
}
   }
