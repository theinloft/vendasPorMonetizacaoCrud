/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vendasPorMonetizacao.negocio;

import java.util.List;
import vendaPorMonetizacao.dao.ClienteDao;
import vendasPorMonetizacao.negocio.NegocioException;
import vendasPorMonetizacao.dao.impl_BD.ClienteDaoBd;
import vendasPorMonetizacao.dominio.Cliente;




/**
 *
 * @author 631520227
 */
public class ClienteNegocio {
    private ClienteDao clienteDao;

    public void salvar(Cliente c) throws NegocioException {
        this.validarCamposObrigatorios(c);
        this.validarCPFExistente(c);
        clienteDao.salvar(c);
    }
   
    
     

    public ClienteNegocio() {
        clienteDao = new ClienteDaoBd();
    }

    public List<Cliente> listar() {
        return (clienteDao.listar());
    }

    public void deletar(Cliente cliente) throws NegocioException {
        if (cliente == null || cliente.getCpf() == null) {
            throw new NegocioException("Cliente nao existe!");
        }
        clienteDao.deletar(cliente);
    }

    public void atualizar(Cliente cliente) throws NegocioException {
        if (cliente == null || cliente.getCpf() == null) {
            throw new NegocioException("Cliente nao existe!");
        }
        this.validarCamposObrigatorios(cliente);
        clienteDao.atualizar(cliente);
    }

    public Cliente procurarPorCPF(String cpf) throws NegocioException {
        if (cpf == null || cpf.isEmpty()) {
            throw new NegocioException("Campo CPF nao informado");
        }
        Cliente cliente = clienteDao.procurarPorCPF(cpf);
        if (cliente == null) {
            throw new NegocioException("cliente nao encontrado");
        }
        return (cliente);
    }

    public List<Cliente> listarPorNome(String nome) throws NegocioException {
        if (nome == null || nome.isEmpty()) {
            throw new NegocioException("Campo nome nao informado");
        }
        return(clienteDao.listarPorNome(nome));
    }

    public boolean clienteExiste(String cpf) {
        Cliente cliente = clienteDao.procurarPorCPF(cpf);
        return (cliente != null);
    }

    private void validarCamposObrigatorios(Cliente c) throws NegocioException {
        if (c.getCpf() == null || c.getCpf().isEmpty()) {
            throw new NegocioException("Campo CPF nao informado");
        }

        if (c.getNome() == null || c.getNome().isEmpty()) {
            throw new NegocioException("Campo nome nao informado");
        }
    }

    private void validarCPFExistente(Cliente c) throws NegocioException {
        if (clienteExiste(c.getCpf())) {
            throw new NegocioException("CPF ja existente");
        }
    }

    
}

