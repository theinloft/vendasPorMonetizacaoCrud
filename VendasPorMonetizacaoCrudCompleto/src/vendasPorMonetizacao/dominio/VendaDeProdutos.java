/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vendasPorMonetizacao.dominio;

import java.time.LocalDate;
import java.util.List;

/**
 *Venda de produtos com monetização: na venda registra-se o código, a data e hora que foi realizada,
 * o cliente e os produtos e quantidade desses que foi vendida. 
 * O sistema deverá calcular o total e ao finalizar a venda,
 * deverá ser debitado da conta do cliente, caso este tenha crédito.
 * 
 * @author thiago
 */
public class VendaDeProdutos {
    private LocalDate data;
    private LocalDate hora;
    private int codigo;
    private Cliente cliente;
    private List<ItemVenda> itensDeVenda;

    public VendaDeProdutos(LocalDate data, LocalDate hora, int codigo, Cliente cliente, List<ItemVenda> itensDeVenda) {
        this.data = data;
        this.hora = hora;
        this.codigo = codigo;
        this.cliente = cliente;
        this.itensDeVenda = itensDeVenda;
    }

    public List<ItemVenda> getItensDeVenda() {
        return itensDeVenda;
    }

    public void setItensDeVenda(List<ItemVenda> itensDeVenda) {
        this.itensDeVenda = itensDeVenda;
    }

    

    public LocalDate getData() {
        return data;
    }

    public LocalDate getHora() {
        return hora;
    }

    public int getCodigo() {
        return codigo;
    }

    public Cliente getCliente() {
        return cliente;
    }

    

    public void setData(LocalDate data) {
        this.data = data;
    }

    public void setHora(LocalDate hora) {
        this.hora = hora;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    
   
    
    
    
    
}
