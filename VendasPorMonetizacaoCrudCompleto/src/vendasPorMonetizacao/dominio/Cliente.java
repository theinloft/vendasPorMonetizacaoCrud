/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vendasPorMonetizacao.dominio;

/**
 *
 * @author 631520227
 */
public class Cliente {
private int id;    
private String nome;
private String cpf;
private String email;

    public Cliente(String cpf, String nome, String email) {
        this.nome = nome;
        this.email = email;
        this.cpf = cpf;
    }

    public Cliente(int id,String cpf, String nome, String email) {
        this.id = id;
        this.nome = nome;
        this.cpf = cpf;
        this.email = email;
    }


    public String getNome() {
        return nome;
    }

    public String getCpf() {
        return cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getId() {
      return id;  
    }

    public void setId(int id) {
        this.id = id;
    }

    
    
    
    
    
    
    
    
}
