/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  thiago
 * Created: 19/12/2017
 */
CREATE DATABASE lp2;

CREATE TABLE public.produto
(
  id integer NOT NULL DEFAULT nextval('produto_id_seq'::regclass),
  nome character varying(30),
  preco double precision,
  CONSTRAINT produto_pkey PRIMARY KEY (id)
)


CREATE TABLE public.cliente
(
  id integer NOT NULL DEFAULT nextval('cliente_id_seq'::regclass),
  nome character varying(30),
  email character varying(50),
  cpf character varying(14),
  CONSTRAINT cliente_pkey PRIMARY KEY (id)
)