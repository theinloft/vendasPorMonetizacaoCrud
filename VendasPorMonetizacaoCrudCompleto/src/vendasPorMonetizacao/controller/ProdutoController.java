/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vendasPorMonetizacao.controller;

import br.com.senacrs.consultorio.view.PrintUtil;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import vendasPorMonetizacao.VendasPorMonetizacao;
import vendasPorMonetizacao.dominio.Produto;
import vendasPorMonetizacao.negocio.NegocioException;
import vendasPorMonetizacao.negocio.ProdutoNegocio;


/**
 * FXML Controller class
 *
 * @author thiago
 */
public class ProdutoController implements Initializable {
    
    @FXML
     private VBox painelTabelaProduto;
    
    @FXML
    private TableColumn<Produto, String> tableColumnNome;
    
    @FXML
    private TableColumn<Produto,String> tableColumnPreco;
    
    @FXML
    private AnchorPane painelFormularioProduto;
    
    @FXML
    private TextField textFieldNome;
    @FXML
    private TextField textFieldPreco;
   
    
    
    @FXML
    private TableView<Produto> tableViewProdutos;
    private Produto produtoSelecionado;
    private int tela;
    private List<Produto> listaProdutos;
    private ObservableList<Produto> observableListaProdutos;
    private ProdutoNegocio produtoNegocio;
    private AnchorPane PainelPrincipal;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
         produtoNegocio = new ProdutoNegocio();
         
        
         if (tableViewProdutos != null) {
            carregarTableViewProdutos();
        }
    }    

  
    @FXML
    private void tratarBtnCadastrarProduto(ActionEvent event) throws IOException{
         produtoSelecionado = null;
        Stage stage = new Stage();
        
        Parent root = FXMLLoader.load(VendasPorMonetizacao.class.getResource("/vendasPorMonetizacao/view/PainelFormularioProduto.fxml"));
        stage.setScene(new Scene(root));
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initOwner(painelTabelaProduto.getScene().getWindow());
        stage.showAndWait();
        carregarTableViewProdutos();
    
}  
@FXML
private void tratarBtnEditarProduto(ActionEvent event) throws IOException{
     Produto produtoSelecionado = tableViewProdutos.getSelectionModel().getSelectedItem();
        if (produtoSelecionado != null) {
            FXMLLoader loader = new FXMLLoader(VendasPorMonetizacao.class.getResource("/vendasPorMonetizacao/view/PainelFormularioProduto.fxml"));
            Parent root = (Parent) loader.load();

            ProdutoController controller = (ProdutoController) loader.getController();
            controller.setProdutoSelecionado(produtoSelecionado);

            Stage dialogStage = new Stage();
            dialogStage.setScene(new Scene(root));
            dialogStage.initModality(Modality.APPLICATION_MODAL);
            dialogStage.initOwner(painelTabelaProduto.getScene().getWindow());
            dialogStage.showAndWait();
            carregarTableViewProdutos();
        } else {
            PrintUtil.printMessageError("Precisa selecionar um cliente para esta opcao");
        }
}
@FXML
private void tratarBtnRemoverProduto(ActionEvent event) throws IOException{
     Produto produtoSelecionado = tableViewProdutos.getSelectionModel().getSelectedItem();
        if (produtoSelecionado != null) {
            try {
                produtoNegocio.deletar(produtoSelecionado);
                this.carregarTableViewProdutos();
            } catch (NegocioException ex) {
                PrintUtil.printMessageError(ex.getMessage());
            }
        } else {
            PrintUtil.printMessageError("Precisa selecionar um Produto para esta opcao");
        }
}
 private void carregarTableViewProdutos() {

        
        tableColumnNome.setCellValueFactory(new PropertyValueFactory<>("nome"));
        tableColumnPreco.setCellValueFactory(new PropertyValueFactory<>("preco"));
          
 

        listaProdutos = produtoNegocio.listar();

        observableListaProdutos = FXCollections.observableArrayList(listaProdutos);
        tableViewProdutos.setItems(observableListaProdutos);
 }
 
 
 public Produto getProdutoSelecionado() {
        return produtoSelecionado;
    }

    public void setProdutoSelecionado(Produto produtoSelecionado) {
        this.produtoSelecionado = produtoSelecionado;
        textFieldPreco.setText(Double.toString(produtoSelecionado.getPreco()));
        textFieldNome.setText(produtoSelecionado.getNome());
        
    }
    
     
     @FXML
    public void tratarBtnSalvar(ActionEvent event) throws IOException {
        Stage stage = (Stage) painelFormularioProduto.getScene().getWindow();
        
        if(produtoSelecionado == null) //Se for cadastrar
        {
            try {
                produtoNegocio.salvar(new Produto(
                        
                        textFieldNome.getText(), 
                        Double.parseDouble( textFieldPreco.getText())));
                
                stage.close();
            } catch (NegocioException ex) {
                PrintUtil.printMessageError(ex.getMessage());
            }
            
        }
        else
        {
            try {
                produtoSelecionado.setNome(textFieldNome.getText());
                produtoSelecionado.setPreco(Double.parseDouble( textFieldPreco.getText()));
                
                                    
                produtoNegocio.atualizar(produtoSelecionado);
                stage.close();
            } catch (NegocioException ex) {
                PrintUtil.printMessageError(ex.getMessage());
            }

    
    
    




}
    }
    
    
    @FXML
    public void tratarBtnCancelar(ActionEvent event) throws IOException {
        Stage stage = (Stage) painelFormularioProduto.getScene().getWindow();
        stage.close();

    }
    

    

    
    
}
