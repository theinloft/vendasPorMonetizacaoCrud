/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vendasPorMonetizacao.controller;

import br.com.senacrs.consultorio.view.PrintUtil;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import vendasPorMonetizacao.VendasPorMonetizacao;
import vendasPorMonetizacao.dominio.Cliente;
import vendasPorMonetizacao.negocio.ClienteNegocio;
import vendasPorMonetizacao.negocio.NegocioException;


/**
 * FXML Controller class
 *
 * @author thiago
 */
public class ClienteController implements Initializable {

     @FXML
    private VBox PainelTabelaCliente1;
     
     @FXML
    private TableView<Cliente> tableViewClientes;
    
     @FXML
    private TableColumn<Cliente, String> tableColumnCpf;
    @FXML
    private TableColumn<Cliente, String> tableColumnNome;
    
    @FXML
    private TableColumn<Cliente,String> tableColumnEmail;
    
    @FXML
    private AnchorPane painelFormularioCliente1;
    @FXML
    private TextField textFieldCpf;
    @FXML
    private TextField textFieldNome;
    @FXML
    private TextField textFieldEmail;
   
    
   
    private Cliente clienteSelecionado;
    private int tela;
    private List<Cliente> listaClientes;
    private ObservableList<Cliente> observableListaClientes;
    private ClienteNegocio clienteNegocio;
    
    
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        clienteNegocio = new ClienteNegocio();
         
        
         if (tableViewClientes != null) {
            carregarTableViewClientes();
        }
        
    }
@FXML
private void tratarBtnCadastrar(ActionEvent event) throws IOException{
         clienteSelecionado = null;
        Stage stage = new Stage(); 
        Parent root = FXMLLoader.load(VendasPorMonetizacao.class.getResource("/vendasPorMonetizacao/view/PainelFormularioCliente1.fxml"));
        stage.setScene(new Scene(root));
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initOwner(PainelTabelaCliente1.getScene().getWindow());
        stage.showAndWait();
        carregarTableViewClientes();
    
}
@FXML
private void tratarBtnEditar(ActionEvent event) throws IOException{
     Cliente clienteSelecionado = tableViewClientes.getSelectionModel().getSelectedItem();
        if (clienteSelecionado != null) {
            FXMLLoader loader = new FXMLLoader(VendasPorMonetizacao.class.getResource("/vendasPorMonetizacao/view/PainelFormularioCliente1.fxml"));
            Parent root = (Parent) loader.load();

            ClienteController controller = (ClienteController) loader.getController();
            controller.setClienteSelecionado(clienteSelecionado);

            Stage dialogStage = new Stage();
            dialogStage.setScene(new Scene(root));
            dialogStage.initModality(Modality.APPLICATION_MODAL);
            dialogStage.initOwner(PainelTabelaCliente1.getScene().getWindow());
            dialogStage.showAndWait();
            carregarTableViewClientes();
        } else {
            PrintUtil.printMessageError("Precisa selecionar um cliente para esta opcao");
        }
}
@FXML
private void tratarBtnRemover(ActionEvent event) throws IOException{
     Cliente clienteSelecionado = tableViewClientes.getSelectionModel().getSelectedItem();
        if (clienteSelecionado != null) {
            try {
                clienteNegocio.deletar(clienteSelecionado);
                this.carregarTableViewClientes();
            } catch (NegocioException ex) {
                PrintUtil.printMessageError(ex.getMessage());
            }
        } else {
            PrintUtil.printMessageError("Precisa selecionar um Cliente para esta opcao");
        }
}
 private void carregarTableViewClientes() {

        tableColumnCpf.setCellValueFactory(new PropertyValueFactory<>("cpf"));
        tableColumnNome.setCellValueFactory(new PropertyValueFactory<>("nome"));
        tableColumnEmail.setCellValueFactory(new PropertyValueFactory<>("email"));
          
 

        listaClientes = clienteNegocio.listar();

        observableListaClientes = FXCollections.observableArrayList(listaClientes);
        tableViewClientes.setItems(observableListaClientes);
 }
 
 
 public Cliente getClienteSelecionado() {
        return clienteSelecionado;
    }

    public void setClienteSelecionado(Cliente clienteSelecionado) {
        this.clienteSelecionado = clienteSelecionado;
        textFieldCpf.setText(clienteSelecionado.getCpf());
        textFieldCpf.setEditable(false);
        textFieldNome.setText(clienteSelecionado.getNome());
        textFieldEmail.setText(clienteSelecionado.getEmail());
    }


    
    
     @FXML
    public void tratarBtnSalvar(ActionEvent event) throws IOException {
        Stage stage = (Stage) painelFormularioCliente1.getScene().getWindow();
        
        if(clienteSelecionado == null) //Se for cadastrar
        {
            try {
                clienteNegocio.salvar(new Cliente(
                        textFieldCpf.getText(), 
                        textFieldNome.getText(), 
                        textFieldEmail.getText()));
                
                stage.close();
            } catch (NegocioException ex) {
                PrintUtil.printMessageError(ex.getMessage());
            }
            
        }
        else
        {
            try {
                clienteSelecionado.setNome(textFieldNome.getText());
                clienteSelecionado.setCpf(textFieldCpf.getText());
                clienteSelecionado.setEmail(textFieldEmail.getText());
                                    
                clienteNegocio.atualizar(clienteSelecionado);
                stage.close();
            } catch (NegocioException ex) {
                PrintUtil.printMessageError(ex.getMessage());
            }

    
    
    




}
    }
    
    
    @FXML
    public void tratarBtnCancelar(ActionEvent event) throws IOException {
        Stage stage = (Stage) painelFormularioCliente1.getScene().getWindow();
        stage.close();

    }
    
    
    

    
    
}
