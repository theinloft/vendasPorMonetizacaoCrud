/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vendasPorMonetizacao.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author thiago
 */
public class PainelPrincipalController implements Initializable {
    @FXML
    AnchorPane painelPrincipal;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    
  @FXML  
   private void irParaProdutos(ActionEvent event) throws IOException{
          Parent PainelTabelaProduto = FXMLLoader.load(this.getClass().getResource("/vendasPorMonetizacao/view/PainelTabelaProduto.fxml"));
          Stage janela = (Stage) painelPrincipal.getScene().getWindow();
        janela.setScene(new Scene(PainelTabelaProduto));
    }
   @FXML  
   private void irParaClientes(ActionEvent event) throws IOException{
          Parent PainelTabelaCliente1 = FXMLLoader.load(this.getClass().getResource("/vendasPorMonetizacao/view/PainelTabelaCliente1.fxml"));
          Stage janela = (Stage) painelPrincipal.getScene().getWindow();
        janela.setScene(new Scene(PainelTabelaCliente1));
    }
   
}
