/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vendaPorMonetizacao.dao;

import java.util.List;
import vendasPorMonetizacao.dominio.Produto;

/**
 *
 * @author thiago
 */
public interface ProdutoDao extends DaoProduto<Produto> {

  public List<Produto> listarPorIdProdutos();
    public List<Produto> listarPorNome(String nome);  
}
