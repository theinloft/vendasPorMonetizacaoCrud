/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vendaPorMonetizacao.dao;

import java.util.List;

/**
 *
 * @author thiago
 */
interface DaoProduto<T> {
     public void salvar(T dominio);
    public void deletar(T produto);
    public void atualizar(T produto);
    public List<T> listar();
    public T procurarPorId(int codigo);
}
