/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vendaPorMonetizacao.dao;

import java.util.List;

/**
 *
 * @author thiago
 */

//A ideia da interface Dao é que padronizar todos os métodos do CRUD da aplicação.
public interface Dao<T> {
    public void salvar(T dominio);
    public void deletar(T cliente);
    public void atualizar(T cliente);
    public List<T> listar();
    public T procurarPorId(int id);
    
}

