/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vendaPorMonetizacao.dao;

import java.util.List;
import vendasPorMonetizacao.dominio.Cliente;

/**
 *
 * @author thiago
 */
public interface ClienteDao extends Dao<Cliente> {
    public Cliente procurarPorCPF(String cpf);
    public List<Cliente> listarPorNome(String nome);
    
    
}
